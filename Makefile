VERSION := $(shell pwd | sed -e 's/.*MUMail\.//')

VERSION_MAJOR := $(shell echo $(VERSION) | awk -F. '{print $$1 "." $$2}')

#VPATH = mumail:mumail/gui:mumail/mime:mumail/net:musoft/utils

CURDIR := $(shell pwd)

SOURCE := $(shell find . -name "*.java" -print)

CLASS := $(patsubst %.java,%.class,$(SOURCE))

RELEASE =	MUMail.jar \
		MUMail.html \
		RELEASENOTES.html \
		MUMail.gif \
		Title.gif \
		Screenshot.gif \
		gpl.html \
		philosophical-gnu-sm.jpg

ARCMDS = 	mkdir tmp; mkdir tmp/MUMail; \
		\cp $(RELEASE) tmp/MUMail; \
		(cd tmp;zip -R MUMail.zip MUMail/\*); \
		(cd tmp;tar cf MUMail.tar MUMail;gzip MUMail.tar); \
		(cd tmp;tar cf MUMail.tar MUMail;bzip2 MUMail.tar); \
		tar cf MUMail.src.tar $(shell find . -name "*.java" -print);\
		gzip -f MUMail.src.tar ;\
		\mv tmp/MUMail.tar.gz tmp/MUMail.zip tmp/MUMail.tar.bz2 .; \
		\rm -r tmp

.PHONY: all
all: MUMail.jar

MUMail.jar: $(CLASS)
	jar cf MUMail.jar $(shell find . -name "*.class" -print | sed -e "s/\\$$/\\\\\\$$/g" )

%.class: %.java
	javac -d . $<

.PHONY: archives1
archives1: $(RELEASE)
	@echo creating archives: stage 1
	-@$(ARCMDS)

.PHONY: archives2
archives2: archives1
	echo patching RELEASENOTES.html
	cat html/RELEASENOTES.html | \
		sed -e 's/%VERSION_MAJOR%/$(VERSION_MAJOR)/g' | \
		sed -e 's/%VERSION_MINOR%/$(VERSION)/g' | \
		sed -e 's/%JAR_SIZE%/\
			$(shell du -k MUMail.jar | awk '{print $$1}')/' | \
		sed -e 's/%BZIP_SIZE%/\
			$(shell du -k MUMail.tar.bz2 | awk '{print $$1}')/' | \
		sed -e 's/%TAR_SIZE%/\
			$(shell du -k MUMail.tar.gz | awk '{print $$1}')/' | \
		sed -e 's/%ZIP_SIZE%/\
			$(shell du -k MUMail.zip | awk '{print $$1}')/' | \
		sed -e 's/%SRC_SIZE%/\
			$(shell du -k MUMail.src.tar.gz | awk '{print $$1}')/' > \
		./RELEASENOTES.html
	@echo creating archives: stage 2
	-@$(ARCMDS)

.PHONY: archives
archives: archives2

.PHONY: install
install: archives
	\cp MUMail.tar.gz $(HOME)/www
	(cd $(HOME)/www; gunzip MUMail.tar.gz; tar xf MUMail.tar; gzip MUMail.tar ;\
	\mv MUMail/RELEASENOTES.html MUMail/index.html; \
	\mv MUMail.tar.gz MUMail/MUMail.$(VERSION).tar.gz)
	\mv MUMail.zip $(HOME)/www/MUMail/MUMail.$(VERSION).zip
	\mv MUMail.tar.bz2 $(HOME)/www/MUMail/MUMail.$(VERSION).tar.bz2
	\mv MUMail.src.tar.gz $(HOME)/www/MUMail/MUMail.$(VERSION).src.tar.gz

.PHONY: test
test: MUMail.jar
	java mumail.MUMail -protocolSelect -pophost localhost -popport 143\
	-smtphost mail.deteline.de

.PHONY: apptest
apptest: MUMail.jar
	(cd /; appletviewer -J-Djava.compiler=nojit $(CURDIR)/MUMail.html)

.PHONY: edit
edit: $(SOURCE)
	emacs $(SOURCE) &

.PHONY: clean
clean:
	find . -name "*.class" -exec \rm {} \;
	find . -name "*~" -exec \rm {} \;
	find . -name "#*#" -exec \rm {} \;
	-\rm -r MUMail.src.tar.gz MUMail.tar.bz2 \
	MUMail.tar.gz MUMail.jar MUMail.zip

tar:	clean
	(cd ..; tar cf - MUMail.$(VERSION) | gzip -9 -c > tars/MUMail.$(VERSION).tgz )

src.tar: clean
	gtar czf src.tgz $(SOURCE)
	mv src.tgz ~/tmp
