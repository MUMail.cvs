/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.gui;

import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.io.IOException;
import mumail.mime.*;
import mumail.net.*;

public class MailPanel extends Panel implements ItemListener, ActionListener, PropertyChangeListener, MailListener{

  MyList HeaderList = new MyList(mumail.MUMail.headerListRows, false);
  ButtonPanel MyButtonPanel;
  Panel MyBodyPanel = new Panel();
  //DownloadStatusPanel downloadStatusPanel = new DownloadStatusPanel();
  StatusPanel statusPanel = new StatusPanel();
  MailQueue mq;
  public mimeMail mo;
  private int downloadInProgress = -1;
  ActionListener actionListener;
  Button cancelDownload = new Button("Cancel");
  boolean downloading = false;

  public MailPanel(MailQueue mq, ActionListener listener) throws IOException {
    super();
    this.mq = mq;
    mq.addPropertyChangeListener(this);
    mq.addPropertyChangeListener(statusPanel);

    MyButtonPanel = new ButtonPanel(listener);
    MyButtonPanel.setConnected(mq.isConnected());

    MyBodyPanel.setLayout(new BorderLayout(0,0));

    GridBagLayout gb = new GridBagLayout();
    GridBagConstraints  gc = new GridBagConstraints();

    setLayout(gb);

    gc.fill=GridBagConstraints.HORIZONTAL;
    gc.insets=new Insets(0,0,0,0);
    gc.ipadx=0;
    gc.ipady=0;
    gc.anchor=GridBagConstraints.CENTER;
    gc.weightx=0.0;
    gc.weighty=0.0;
    gc.gridwidth=GridBagConstraints.REMAINDER;
    gc.gridheight=1;

    gb.setConstraints(HeaderList, gc);
    add(HeaderList);

    gb.setConstraints(MyButtonPanel, gc);
    add(MyButtonPanel);

    gc.fill=GridBagConstraints.BOTH;
    gc.weighty=1.0;
    gc.weightx=1.0;

    gb.setConstraints(MyBodyPanel, gc);
    add(MyBodyPanel);

    gc.gridheight=GridBagConstraints.REMAINDER;
    gc.fill=GridBagConstraints.HORIZONTAL;
    gc.weighty=0.0;
    gc.weightx=1.0;

    gb.setConstraints(statusPanel, gc);
    add(statusPanel);

    this.addActionListener(listener);

    HeaderList.addItemListener(this);
    MyButtonPanel.DeleteButton.addActionListener(this);
    MyButtonPanel.CheckNewButton.addActionListener(this);
    MyButtonPanel.CloseButton.addActionListener(this);

    //setBackground(Color.red);

    cancelDownload.setActionCommand("I18N_CANCEL_DOWNLOAD");
    cancelDownload.addActionListener(this);
  }

  public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
    //System.out.print("Got event: ");
    if (propertyChangeEvent.getSource() == mq) {
      if (propertyChangeEvent.getPropertyName().equals(mq.pn_Connected)) {
        MyButtonPanel.setConnected(mq.isConnected());
        if (mq.isConnected()) {
          System.out.println("Connected");
          try {
            DisplayHeaders();
          } catch (IOException e1) {
            e1.printStackTrace();
          }
        } else {
          System.out.println("Disconnected");
        }
      }
    }
  }

  public synchronized void setDownloading(boolean isDownloading) {
    if (downloading != isDownloading) {
      if (isDownloading) {
        MyButtonPanel.getCustomContainer().add(cancelDownload);
      } else {
        MyButtonPanel.getCustomContainer().remove(cancelDownload);
      }
      HeaderList.setEnabled(!isDownloading);
      downloading = isDownloading;
    }
  }

  public void DisplayHeaders() throws IOException {

    int i;
    mimeHeader mh;
    String out;
    String fromout;
    String subjectout;
    int fromwidth=0, subjectwidth=0;
    int wasSelected = HeaderList.getSelectedIndex();
    int messageCount = mq.getCount();
    if(HeaderList.getItemCount()!=0){
      //System.out.println("clearing list");
      HeaderList.removeAll();
    }

    FontMetrics fm = HeaderList.getFontMetrics(HeaderList.getFont());

    if(fm!=null){
      for(i = 0; i < messageCount; i++){
      	fromwidth=Math.max(fromwidth,fm.stringWidth(mq.getHeader(i).getFrom()));
	subjectwidth=Math.max(subjectwidth,fm.stringWidth(mq.getHeader(i).getSubject()));
      }
    }
    for(i=0; i < messageCount; i++){
      mh = mq.getHeader(i);
      fromout = mh.getFrom();
      subjectout = mh.getSubject();
      if(fm!=null){
	while(fm.stringWidth(fromout)<fromwidth){
	  fromout = fromout +" ";
	}
	while(fm.stringWidth(subjectout)<subjectwidth){
	  subjectout = subjectout +" ";
	}
      }
      out = "  " + fromout + " | " + subjectout + " | " + mh.getDate();
      HeaderList.add(out);
    }
    wasSelected = Math.min(wasSelected, messageCount - 1);
    if(wasSelected != -1) {
      HeaderList.makeVisible(wasSelected);
      HeaderList.select(wasSelected);
      // vvvvv This should be generated by the select(...) call vvvvv
      itemStateChanged(new ItemEvent(HeaderList,
                                     wasSelected,
                                     Integer.toString(wasSelected),
                                     ItemEvent.SELECTED));
      // ^^^^^ This should be generated by the select(...) call ^^^^^
    }
  }

  protected synchronized void updateBodyPanel(int index) {
    boolean wasConnected = mq.isConnected();

    try {
      if (!downloading) { // We should only be called while already downloading
                          // due to a connect event.
                          // In this case the selection should not have changed
                          // and we are already downloading the right message!
                          // We just wait for the mailDownloaded event.
        MyBodyPanel.removeAll();
        MyButtonPanel.getCustomContainer().removeAll();
        downloadInProgress = index;
        setDownloading(true);
        mo = mq.getMessage(index, this);
        if (mo != null) {
          setDownloading(false);
          downloadInProgress = -1;
          MyButtonPanel.getCustomContainer().add(mo.getCustomPanel());
          MyBodyPanel.add("Center", mo);
          MyButtonPanel.setMessageSelected(true);
        }
      }
    } catch(Exception exep) {
      MyButtonPanel.setMessageSelected(false);
      if (wasConnected) {
        System.out.println("exception: " + exep.getMessage());
        exep.printStackTrace();
      } else {
        System.out.println("You are not connected!");
      }
    }
    validate();
  }

  public void downloadStatusUpdated(int percentDone) {
    if(downloading){
      statusPanel.downloadStatusUpdated(percentDone);
    }
  }

  public synchronized void mailDownloaded(int index) {
    statusPanel.mailDownloaded(index);
    setDownloading(false);
    try {
      if (downloadInProgress == index) {
        updateBodyPanel(index);
      }
    } catch(Exception exep) {
      exep.printStackTrace();
    }
  }

  public synchronized void downloadFailed(int index, Exception e) {
    statusPanel.downloadFailed(index, e);
    setDownloading(false);
    HeaderList.deselect(HeaderList.getSelectedIndex());
    if (!(e instanceof InterruptedException)) {
      e.printStackTrace();
    }
  }

  public synchronized void itemStateChanged(ItemEvent e) {
    if (e.getStateChange() == ItemEvent.SELECTED) {
      int index = Integer.parseInt(e.getItem().toString());
      updateBodyPanel(index);
    }
  }

  public void actionPerformed(ActionEvent e) {
    if(e.getSource() == MyButtonPanel.DeleteButton){
      try {
        mq.deleteMessage(HeaderList.getSelectedIndex());
      } catch (IOException e1) {
        e1.printStackTrace();
      }
    } else if(e.getSource() == MyButtonPanel.CheckNewButton){
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      if (mo != null)
        MyBodyPanel.remove(mo);
      MyButtonPanel.ReplyButton.setEnabled(false);
      try{
        mq.reConnect();
      } catch (IOException excep){
        processActionEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "Close"));
      }
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    } else if(e.getSource() == cancelDownload){
      mq.abortCurrentDownload();
    }
  }

  public Dimension getPreferredSize() {
    return getParent().getSize();
  }

  public synchronized void addActionListener(ActionListener l) {
    actionListener = AWTEventMulticaster.add(actionListener, l);
  }

  public synchronized void removeActionListener(ActionListener l) {
    actionListener = AWTEventMulticaster.remove(actionListener, l);
  }

  protected void processActionEvent(ActionEvent e) {
    if (actionListener != null) {
      actionListener.actionPerformed(e);
    }
  }

}

