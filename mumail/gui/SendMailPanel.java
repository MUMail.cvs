/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.gui;

import java.awt.*;
import java.awt.event.*;
import mumail.mime.*;

public class SendMailPanel extends Panel implements ActionListener, TextListener, ItemListener{

  static final String MIMEcharSets[] = {"ISO-8859-1",
                                         "ISO-8859-2", 
                                         "ISO-8859-3", 
                                         "ISO-8859-4", 
                                         "ISO-8859-6", 
                                         "ISO-8859-7", 
                                         "ISO-8859-8", 
                                         "ISO-8859-5", 
                                         "ISO-8859-9", 
                                         "ISO-2022-KR", 
                                         "ISO-2022-JP", 
                                         "ISO-2022-JP-2", 
                                         "EUC-KR", 
                                         "KOI8-R", 
                                         "Shift_JIS", 
                                         "EUC-JP", 
                                         "GB2312", 
                                         "Big5"};

  Choice EncodingChoice = new Choice();
  Choice FontChoice = new Choice();

  public TextField From = new TextField();
  public TextField To = new TextField();
  public TextField Subject = new TextField();
  public TextArea Text = new TextArea("", 0, 0, TextArea.SCROLLBARS_VERTICAL_ONLY);

  Button Send = new Button("SendMail");
  Button Cancel = new Button("Cancel");
  Button Clear = new Button("Clear");

  public SendMailPanel(String from, ActionListener listener){
    super();
    HelpConstructor(from, listener);
    initDisplay();
  }

  public void HelpConstructor(String from, ActionListener listener){
    Send.addActionListener(listener);
    Cancel.addActionListener(listener);
    From.setText(from);

    if(mumail.MUMail.authorizedSend){
      From.setEnabled(false);
    }

    if(mumail.MUMail.showFontChoice){
      for(int i = 0; i < MIMEcharSets.length; i++) {
        if (mumail.mime.CharacterEncoding.aliasName(MIMEcharSets[i]) != null) {
          EncodingChoice.add(MIMEcharSets[i]);
        }
      }

      String[] fontlist = Toolkit.getDefaultToolkit().getFontList();
      for(int i=0;i<fontlist.length;i++){
        FontChoice.add(fontlist[i]);
      }
      FontChoice.addItemListener(this);
     }
        
  }

  public SendMailPanel(mimeMail mo, String from, ActionListener listener){
    super();
    HelpConstructor(from, listener);
    To.setText(mo.getHeader().getFrom());
    Subject.setText("Re: "+mo.getHeader().getSubject());
    if(mo.getBody() instanceof MIMEtext){
      Text.append("On "+mo.getHeader().getDate()+", "+mo.getHeader().getFrom()+" wrote:\n");
      Text.append("\n");
      insertText((MIMEtext)mo.getBody());
    }
    if(mo.getBody() instanceof MIME_multipart){
      for(int i=0;i<((MIME_multipart)mo.getBody()).getPartCount();i++){
	if(((MIME_multipart)mo.getBody()).getPart(i) instanceof MIMEtext){
	  Text.append("On "+mo.getHeader().getDate()+", "+mo.getHeader().getFrom()+" wrote:\n");
	  Text.append("\n");
	  insertText((MIMEtext)((MIME_multipart)mo.getBody()).getPart(i));
	  break;
	}
      }
    }
    initDisplay();
  }



  private void insertText(MIMEtext mo){
    String[] lines = mo.getLines();
    for(int j=0;j<lines.length;j++){
      Text.append(">"+lines[j]+"\n");
    }
  }

  public void initDisplay(){
    Label ToLabel = new Label("To:");
    Label SubjectLabel = new Label("Subject:");
    Label FromLabel = new Label("From:");

    GridBagLayout gb = new GridBagLayout();
    GridBagConstraints gc = new GridBagConstraints();

    setLayout(gb);
		
    //first make a extra panel that contains labels and textfields
    //we need this due to insets between components
    Panel p = new Panel();
    p.setLayout(gb);

    gc.insets = new Insets(5,5,5,5); //!!!
    gc.fill = GridBagConstraints.NONE;
    gc.anchor = GridBagConstraints.EAST;
    gc.weightx=0.0;
    gc.weighty=0.0;
    gc.ipadx=0;
    gc.ipady=0;
    gc.gridheight=1;

    gb.setConstraints(FromLabel, gc);
    p.add(FromLabel);
		
    gc.fill = GridBagConstraints.HORIZONTAL;
    gc.anchor = GridBagConstraints.WEST;
    gc.gridwidth = GridBagConstraints.REMAINDER;
    gc.weightx = 1.0;

    gb.setConstraints(From, gc);
    p.add(From);
		
    gc.gridwidth = 1;
    gc.fill = GridBagConstraints.NONE;
    gc.anchor = GridBagConstraints.EAST;
    gc.weightx=0.0;

    gb.setConstraints(ToLabel, gc);
    p.add(ToLabel);
		
    gc.fill = GridBagConstraints.HORIZONTAL;
    gc.anchor = GridBagConstraints.WEST;
    gc.gridwidth = GridBagConstraints.REMAINDER;
    gc.weightx = 1.0;

    gb.setConstraints(To, gc);
    p.add(To);
		
    gc.fill = GridBagConstraints.NONE;
    gc.anchor = GridBagConstraints.EAST;
    gc.gridwidth = 1;
    gc.weightx = 0.0;

    gb.setConstraints(SubjectLabel, gc);
    p.add(SubjectLabel);

    gc.fill = GridBagConstraints.HORIZONTAL;
    gc.anchor = GridBagConstraints.WEST;
    gc.gridwidth = GridBagConstraints.REMAINDER;
    gc.weightx = 1.0;

    gb.setConstraints(Subject, gc);
    p.add(Subject);

    //ok, from here we adding in this
    gc.insets = new Insets(0,0,0,0); //!!!
    gc.fill = GridBagConstraints.HORIZONTAL;
    gc.anchor = GridBagConstraints.CENTER;
    gc.weightx=1.0;
    gc.gridwidth = GridBagConstraints.REMAINDER;

    gb.setConstraints(p, gc);
    add(p);

    Panel bp = new Panel();
    bp.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
    bp.add(Send);
    bp.add(Cancel);
    bp.add(Clear);
    if(mumail.MUMail.showFontChoice){
      bp.add(FontChoice);
      bp.add(EncodingChoice);
    }
    bp.setBackground(Color.gray);

    gb.setConstraints(bp, gc);
    add(bp);

    gc.fill = GridBagConstraints.BOTH;
    gc.gridheight = GridBagConstraints.REMAINDER;
    gc.weightx = 1.0;
    gc.weighty = 1.0;

    gb.setConstraints(Text, gc);
    add(Text);

    Clear.addActionListener(this);
    From.addTextListener(this);
    To.addTextListener(this);
    Send.setEnabled(!(From.getText().equals("") || To.getText().equals("")));
  }

  public String getEncoding() {
	if(!mumail.MUMail.showFontChoice){
		return MIMEcharSets[0];
	}
	else{
	    return EncodingChoice.getSelectedItem();
	}
  }

  public Dimension getPreferredSize() {
    return getParent().getSize();
  }

  public void actionPerformed(ActionEvent e){
    Text.replaceRange("", 0, Text.getText().length());
  }

  public void textValueChanged(TextEvent e){
    Send.setEnabled(!(From.getText().equals("") || To.getText().equals("")));
  }
  
  public void itemStateChanged(ItemEvent e){
    if(e.getSource()==FontChoice){
      Text.setFont(new Font(FontChoice.getSelectedItem(), Text.getFont().getStyle(), Text.getFont().getSize()));
    }
  }

}
