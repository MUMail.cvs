/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.gui;

import java.awt.*;

public class ProgressBar extends Canvas{

  private int percentage = -1;
  private Font progressFont;

  public ProgressBar(){
    //setBackground(Color.blue);
  }

  public Dimension getPreferredSize(){
    return new Dimension(102,16);
  }

  public Dimension getMaximumSize(){
    return getPreferredSize();
  }

  public Dimension getMinimumSize(){
    return getPreferredSize();
  }

  public void setPercentage(int percentage){
    this.percentage = percentage;
    repaint();
  }

  public void paint(Graphics g){
    Image offScreen = createImage(getSize().width, getSize().height);
    Graphics g2 = offScreen.getGraphics();
    super.paint(g2);
    g2.setColor(Color.black);
    g2.drawRect(0, 0, 101, 15);
    if(percentage >= 0 && percentage <= 100){
      g2.setColor(Color.lightGray);
      g2.fillRect(1, 1, percentage, 14);
      g2.setColor(Color.black);
      if (progressFont == null) {
        progressFont = new Font(getFont().getName(), getFont().getStyle(), 10);
      }
      g2.setFont(progressFont);
      String p = percentage+"%";
      int w = getFontMetrics(g2.getFont()).stringWidth(p);
      g2.drawString(p, 55-w/2, 12);
    }

    g.drawImage(offScreen, 0, 0, this);
  }

}
