/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.gui;

import java.awt.*;

public class StatusPanel extends Panel implements java.beans.PropertyChangeListener, mumail.net.MailListener{

  Label connectStatus = new Label("Offline");

  LED led = new LED();

  ProgressBar progressBar = new ProgressBar();

  public StatusPanel(boolean connected){
    initDisplay(connected);
  }

  public StatusPanel(){
    initDisplay(false);
  }

  private void initDisplay(boolean connected){
    setLayout(new FlowLayout(FlowLayout.LEFT));

    setBackground(Color.gray);

    add(led);
    add(connectStatus);
    add(progressBar);

    setConnectStatusLabel(connected);
  }    

  public void propertyChange(java.beans.PropertyChangeEvent e){
    if(e.getPropertyName().equals(mumail.net.MailQueue.pn_Connected)){
      setConnectStatusLabel(((Boolean)e.getNewValue()).booleanValue());
    }
  }

  private void setConnectStatusLabel(boolean connected){
    if(!connected){
      connectStatus.setText("Online");
      led.setForeground(new Color(0,128,0));
    }
    else{
      connectStatus.setText("Offline");
      led.setForeground(Color.lightGray);
    }
    led.repaint();
  }

  public void mailDownloaded(int index){
    progressBar.setPercentage(-1);
  }

  public void downloadFailed(int index, Exception e){
    progressBar.setPercentage(-1);
  }

  public void downloadStatusUpdated(int percentage){
    progressBar.setPercentage(percentage);
  }

}
