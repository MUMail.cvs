/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.gui;

import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import musoft.utils.Timer;

public class SplashPanel extends Panel implements ImageObserver{

  Image image;
  Timer timer;

  public SplashPanel(Image image, Timer timer){
    this.timer = timer;
    init(image);
  }

  public boolean imageUpdate(Image img, int flags, int x, int y, int w, int h){
    if((flags&ImageObserver.ALLBITS)!=0){
      timer.start();
    }
    return super.imageUpdate(img, flags, x, y, w, h);
  }

  public void init(Image image){
    this.image = image;
  }

  public Dimension getPreferredSize() {
    return getParent().getSize();
  }

  public void paint(Graphics g){
    int x = image.getWidth(this);
    int y = image.getHeight(this);

    g.drawImage(this.image, (getSize().width-x)/2, (getSize().height-y)/2, null);
  }
  
}
