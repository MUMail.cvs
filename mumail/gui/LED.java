/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.gui;

import java.awt.*;

public class LED extends Canvas{

  public LED(){
  }

  public Dimension getPreferredSize(){
    return new Dimension(16,16);
  }

  public Dimension getMaximumSize(){
    return getPreferredSize();
  }

  public Dimension getMinimumSize(){
    return getPreferredSize();
  }

  public void paint(Graphics g){
    super.paint(g);
    g.setColor(Color.black);
    g.fillOval(0, 0, getSize().width-1, getSize().height-1);
    g.setColor(getForeground());
    g.fillOval(1, 1, getSize().width-3, getSize().height-3);
  }

}
