/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.gui;

import java.awt.*;
import java.beans.*;

public class ButtonPanel extends Panel {

  private boolean connected = false;
  private boolean messageSelected = false;

  public Panel left = new Panel();
  public Panel center = new Panel();
  public Panel right = new Panel();
  public Button CheckNewButton = new Button("CheckNew");
  public Button DeleteButton = new Button("Delete");
  public Button ReplyButton = new Button("Reply");
  public Button SendButton = new Button("Send");
  public Button CloseButton = new Button("Close");
  public Button ExitButton = new Button("Exit");
  public Button ConnectionButton = new Button("Online");
  Panel customContainer = new Panel();

  public ButtonPanel (java.awt.event.ActionListener listener) {
    left.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
    center.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
    right.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
    left.add(SendButton);
    left.add(ReplyButton);
    ReplyButton.setEnabled(false);
    DeleteButton.setEnabled(false);
    left.add(DeleteButton);
    CheckNewButton.setEnabled(false);
    left.add(CheckNewButton);
    // for visual debugging
    // customContainer.setBackground(Color.green);
    customContainer.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
    center.add(customContainer);
    right.add(ConnectionButton);
    right.add(CloseButton);
    //add(PrintButton);
    if (!mumail.MUMail.applet) {
      ExitButton.setActionCommand("Exit2");
      right.add(ExitButton);
    }

    setLayout(new BorderLayout());
    add(left, BorderLayout.WEST);
    add(center, BorderLayout.CENTER);
    add(right, BorderLayout.EAST);
    setBackground(Color.gray);

    SendButton.addActionListener(listener);
    ReplyButton.addActionListener(listener);
    CloseButton.addActionListener(listener);
    ExitButton.addActionListener(listener);
    ConnectionButton.setActionCommand("Connection");
    ConnectionButton.addActionListener(listener);
  }

  public void setMessageSelected(boolean isMessageSelected) {
    messageSelected = isMessageSelected;
    ReplyButton.setEnabled(isMessageSelected);
    if (connected) {
      DeleteButton.setEnabled(isMessageSelected);
    }
  }

  public void setConnected(boolean isConnected) {
    connected = isConnected;
    if (connected) {
      ConnectionButton.setLabel("Offline");
      CheckNewButton.setEnabled(true);
      if (messageSelected) {
        DeleteButton.setEnabled(true);
      }
    } else {
      ConnectionButton.setLabel("Online");
      CheckNewButton.setEnabled(false);
      DeleteButton.setEnabled(false);
    }
  }

  public Panel getCustomContainer(){
    return customContainer;
  }
}

