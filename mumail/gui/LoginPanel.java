/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.gui;

import java.awt.*;
import java.awt.event.*;

public class LoginPanel extends Panel implements ActionListener, ItemListener{

  public static final String homepage = "MUMail Homepage";

  private static final String hostLabelName = " Host:";
  private static final String portLabelName = " Port:";
  private static final String clearCommand = "Clear";

  Label ProtocolLabel;
  Label PopHostLabel;
  Label SmtpHostLabel;
  Label PopPortLabel;
  Label SmtpPortLabel;
  Label UserLabel;
  Label PasswordLabel;

  Choice ProtocolC;
  TextField PopHostTF;
  TextField SmtpHostTF;
  TextField PopPortTF;
  TextField SmtpPortTF;
  TextField UserTF;
  public TextField PasswordTF;

  Button ConnectButton = new Button("Connect");
  Button ClearButton = new Button(clearCommand);
  Button SendButton = new Button("Send");
  Button ExitButton = new Button("Exit");
  Button HomepageButton = new Button(homepage);
  
  mumail.net.MailClientFactory factory;

  public LoginPanel(ActionListener listener, mumail.net.MailClientFactory factory) {
    this("", "", "", "", "", "", listener, factory);
  }

  public LoginPanel(String PopHost, String PopPort, String SmtpHost, String SmtpPort, String User, String Password, ActionListener listener, mumail.net.MailClientFactory factory) {

    this.factory = factory;
    ClearButton.addActionListener(this);
    HomepageButton.addActionListener(listener); 
    ConnectButton.addActionListener(listener); 
    SendButton.addActionListener(listener); 
    ExitButton.addActionListener(listener); 

    GridBagLayout gb = new GridBagLayout();
    GridBagConstraints c = new GridBagConstraints();

    ProtocolLabel = new Label("Protocol:", Label.LEFT);
    PopHostLabel = new Label(factory.getMailClientProtocolName(0) + hostLabelName,
			     Label.LEFT);
    SmtpHostLabel = new Label("SMTP Host:", Label.LEFT);
    PopPortLabel = new Label(factory.getMailClientProtocolName(0) + portLabelName,
			     Label.LEFT);
    SmtpPortLabel = new Label("SMTP Port:", Label.LEFT);
    UserLabel = new Label("User:", Label.LEFT);
    PasswordLabel = new Label("Password:", Label.LEFT);

    ProtocolC = new Choice();
    for(int i=0;i<factory.getClientCount();i++){
      ProtocolC.add(factory.getMailClientProtocolName(i));
    }
    ProtocolC.addItemListener(this);
    PopHostTF = new TextField(PopHost, 15);
    SmtpHostTF = new TextField(SmtpHost, 15);
    PopPortTF = new TextField(PopPort, 5);
    SmtpPortTF = new TextField(SmtpPort, 5);
    UserTF = new TextField(User, 15);
    PasswordTF = new TextField(Password, 15);
    PasswordTF.setEchoChar('*');
    PasswordTF.addActionListener(listener);

    this.setLayout(gb);

    c.fill = GridBagConstraints.NONE;
    c.insets = new Insets(5,5,5,5);
    c.weightx = 1.0;
    c.weighty = 1.0;

    if(!mumail.MUMail.novice){

      if(mumail.MUMail.protocolSelect){
	c.anchor = GridBagConstraints.EAST;
	gb.setConstraints(ProtocolLabel, c);
	add(ProtocolLabel);
      
	c.gridwidth = GridBagConstraints.REMAINDER;
	c.anchor = GridBagConstraints.WEST;
	gb.setConstraints(ProtocolC, c);
	add(ProtocolC);		
      }

      c.gridwidth = 1;
      c.anchor = GridBagConstraints.EAST;
      gb.setConstraints(PopHostLabel, c);
      add(PopHostLabel);      

      c.anchor = GridBagConstraints.WEST;
      gb.setConstraints(PopHostTF, c);
      add(PopHostTF);
		
      c.anchor = GridBagConstraints.EAST;
      gb.setConstraints(PopPortLabel, c);
      add(PopPortLabel);
		
      c.gridwidth = GridBagConstraints.REMAINDER;
      c.anchor = GridBagConstraints.WEST;
      gb.setConstraints(PopPortTF, c);
      add(PopPortTF);
    
      c.gridwidth = 1;
      c.anchor = GridBagConstraints.EAST;
      gb.setConstraints(SmtpHostLabel, c);
      add(SmtpHostLabel);
      
      c.anchor = GridBagConstraints.WEST;
      gb.setConstraints(SmtpHostTF, c);
      add(SmtpHostTF);
    
      c.anchor = GridBagConstraints.EAST;
      gb.setConstraints(SmtpPortLabel, c);
      add(SmtpPortLabel);
		
      c.gridwidth = GridBagConstraints.REMAINDER;
      c.anchor = GridBagConstraints.WEST;
      gb.setConstraints(SmtpPortTF, c);
      add(SmtpPortTF);
    }

    c.gridwidth = 1;
    c.anchor = GridBagConstraints.EAST;
    gb.setConstraints(UserLabel, c);
    add(UserLabel);
    
    c.anchor = GridBagConstraints.WEST;
    gb.setConstraints(UserTF, c);
    add(UserTF);
    
    c.anchor = GridBagConstraints.EAST;
    gb.setConstraints(PasswordLabel, c);
    add(PasswordLabel);
		
    c.gridwidth = GridBagConstraints.REMAINDER;
    c.anchor = GridBagConstraints.WEST;
    gb.setConstraints(PasswordTF, c);
    add(PasswordTF);


    
    c.anchor = GridBagConstraints.CENTER;
	  c.fill = GridBagConstraints.NONE;
	  c.gridwidth = 1;
	  gb.setConstraints(ConnectButton, c);
	  add(ConnectButton);
    
	  c.gridwidth = 1;
	  if(!mumail.MUMail.authorizedSend){
	    gb.setConstraints(SendButton, c);
	    add(SendButton);
	  }
	  else{
	    Component comp = new Panel();
	    gb.setConstraints(comp, c);
	    add(comp);
	  }
    
	  c.gridwidth = 1;
	  gb.setConstraints(ClearButton, c);
	  add(ClearButton);
    
	  c.gridwidth = GridBagConstraints.REMAINDER;
	  c.gridheight = GridBagConstraints.REMAINDER;

    if(mumail.MUMail.applet){
  	  gb.setConstraints(HomepageButton, c);
	    add(HomepageButton);
    }
    else{
  	  gb.setConstraints(ExitButton, c);
	    add(ExitButton);
    }


    this.setBackground(Color.lightGray);
    validate();
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals(clearCommand)) {
      PopHostTF.setText("");
      SmtpHostTF.setText("");
      //PopPortTF.setText("");
      UserTF.setText("");
      PasswordTF.setText("");
    }
  }

  public void itemStateChanged(ItemEvent e){
    String protocol = e.getItem().toString();
    for(int i=0;i<factory.getClientCount();i++){
      if(protocol.equals(factory.getMailClientProtocolName(i))){
	PopPortTF.setText(factory.getMailClientDefaultPort(i));
	PopPortLabel.setText(factory.getMailClientProtocolName(i) +
			     " " + portLabelName);
	PopHostLabel.setText(factory.getMailClientProtocolName(i) +
			     " " + hostLabelName);
	this.validate();
	break;
      }
    }
  }

  public String getProtocol(){
    return ProtocolC.getSelectedItem();
  }

  public String getServerHost(){
    return PopHostTF.getText();
  }

  public String getSmtpHost(){
    return SmtpHostTF.getText();
  }

  public String getServerPort(){
    return PopPortTF.getText();
  }

  public String getSmtpPort(){
    return SmtpPortTF.getText();
  }

  public String getUser(){
    return UserTF.getText();
  }

  public String getPassword(){
    return PasswordTF.getText();
  }

  public void setPopHost(String s){
     PopHostTF.setText(s);
  }

  public void setSmtpHost(String s){
     SmtpHostTF.setText(s);
  }

  public void setPopPort(String s){
     PopPortTF.setText(s);
  }

  public void setSmtpPort(String s){
     SmtpPortTF.setText(s);
  }

  public void setUser(String s){
     UserTF.setText(s);
  }

  public void setPassword(String s){
     PasswordTF.setText(s);
  }

}
