/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.gui;

import java.awt.*;

public class HeaderPanel extends Panel{

  static int space = 3;

  String from, subject, date, to;

  public HeaderPanel(String from, String to, String subject, String date){
    this.from=from;
    this.to=to;
    this.subject=subject;
    this.date=date;
  }

  public Dimension getPreferredSize(){
    return new Dimension(getSize().width, space+(getFont().getSize()+space)*4);
  }

  public Dimension getMinimumSize() {
    return this.getPreferredSize();
  }

  public void paint(Graphics g){
    //setFont(new Font(getFont().getName(), Font.BOLD, getFont().getSize()));
    g.drawString("From:", space, getFont().getSize()+space);
    g.drawString(from, 2*space+g.getFontMetrics().stringWidth("From:"), getFont().getSize()+space);
    g.drawString("To:", space, (getFont().getSize()+space)*2);
    g.drawString(to, 2*space+g.getFontMetrics().stringWidth("To:"), (getFont().getSize()+space)*2);
    g.drawString("Subject:", space, (getFont().getSize()+space)*3);
    g.drawString(subject, 2*space+g.getFontMetrics().stringWidth("Subject:"), (getFont().getSize()+space)*3);
    g.drawString("Date:", space, (getFont().getSize()+space)*4);
    g.drawString(date, 2*space+g.getFontMetrics().stringWidth("Date:"), (getFont().getSize()+space)*4);
  }
}

