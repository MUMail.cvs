/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.gui;

import java.awt.*;
import java.awt.event.*;

public class MessagePanel extends Panel{

  public Button ok = new Button("OK");

  public MessagePanel(String s, ActionListener list){

    Label text = new Label(s);
    GridBagLayout l = new GridBagLayout();
    GridBagConstraints c = new GridBagConstraints();

    setLayout(l);

    c.fill = GridBagConstraints.NONE;
    c.insets = new Insets(5,5,5,5);
    c.anchor = GridBagConstraints.CENTER;
    c.weightx = 1.0;
    c.weighty = 1.0;
    c.gridwidth = GridBagConstraints.REMAINDER;
    c.gridheight = 1;
    l.setConstraints(text, c);
    add(text);
    c.gridwidth = GridBagConstraints.REMAINDER;
    c.gridheight = GridBagConstraints.REMAINDER;
    l.setConstraints(ok, c);
    add(ok);
    ok.addActionListener(list);
  }

  public Dimension getPreferredSize() {
    return getParent().getSize();
  }

}
