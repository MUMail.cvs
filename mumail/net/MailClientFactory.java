/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.net;

/**
 *
 **/
public class MailClientFactory{

  /**
   *
   */
  private java.util.Vector mailClientsClassName = new java.util.Vector();

  /**
   *
   */
  private java.util.Vector mailClientsProtocolName = new java.util.Vector();

  /**
   *
   */
  private java.util.Vector mailClientsDefaultPort = new java.util.Vector();

  /**
   *
   */
  public MailClientFactory(String classNameList){
    java.util.StringTokenizer tok =
      new java.util.StringTokenizer(classNameList, ":");
    while(tok.hasMoreElements()){
      String className = tok.nextToken();
      MailClient mc = initMailClient(className);
      if(mc != null){
	System.err.println("registering MailClient " + className);
	mailClientsClassName.addElement(className);
	mailClientsProtocolName.addElement(mc.getProtocolName());
	mailClientsDefaultPort.addElement(mc.getDefaultPort());
      }
    }
  }

  /**
   *
   */
  public int getClientCount(){
    return mailClientsClassName.size();
  }

  /**
   *
   */
  public MailClient getMailClientByProtocolName(String protocolName){
    int index = mailClientsProtocolName.indexOf(protocolName);
    if(index>=0){
      return initMailClient((String)mailClientsClassName.elementAt(index));
    }
    else{
      return null;
    }
  }
  
  /**
   *
   */
  public MailClient getMailClient(String className){
    return initMailClient(className);
  }
  
  /**
   *
   */
  public String getMailClientProtocolName(int i){
    return (String) mailClientsProtocolName.elementAt(i);
  }

  /**
   *
   */
  public String getMailClientDefaultPort(int i){
    return (String) mailClientsDefaultPort.elementAt(i);
  }

  /**
   *
   */
  private MailClient initMailClient(String className){
    Class mailClientClass;

    try{
      mailClientClass = Class.forName(className);
    }catch(ClassNotFoundException excep){
      System.err.println(excep);
      return null;
    }
    
    if(!isMailClient(mailClientClass)){
      System.err.println(className + " does not implement Interface mumail.net.MailClient");
      return null;
    }

    java.lang.reflect.Constructor constructor;
    
    try {
      Class[] argument_types = new Class[0];
      constructor = mailClientClass.getConstructor(argument_types);
    }catch(NoSuchMethodException excep){
      System.err.println(excep);
      return null;
    }

    MailClient mailClient;

    try{
      Object[] arguments = new Object[0];
      mailClient = (MailClient) constructor.newInstance(arguments);
      return mailClient;
    }catch(Exception excep){
      System.err.println(excep);
      return null;
    }

  }

  /**
   *
   */
  private boolean isMailClient(Class classClass){
    Class interfaceClass;
    try{
      interfaceClass = Class.forName("mumail.net.MailClient");
    }catch(ClassNotFoundException excep){
      return false;
    }
    Class[] classClassInterfaces = classClass.getInterfaces();

    for(int i=0;i<classClassInterfaces.length;i++){
      if(classClassInterfaces[i].equals(interfaceClass)){
        return true;
      }
    }

    Class superClass = classClass.getSuperclass();

    if(superClass!=null){
      return isMailClient(superClass);
    }
    else{
      // root of class hierarchy
      return false;
    }
  }

}
