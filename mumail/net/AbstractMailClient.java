/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is" distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.net;

import java.beans.*;
import java.io.*;

/**
 *
 **/
public abstract class AbstractMailClient implements MailClient{

  /**
   *
   **/
  private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

  /**
   *
   **/
  boolean Connected = false;

  /**
   *
   **/
  public void addPropertyChangeListener(PropertyChangeListener listener){
    propertyChangeSupport.addPropertyChangeListener(listener);
  }

  /**
   *
   **/
  public void removePropertyChangeListener(PropertyChangeListener listener){
    propertyChangeSupport.removePropertyChangeListener(listener);
  }

  /**
   *
   **/
  protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
    propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
  }

  /**
   *
   **/
  void setConnected(boolean isConnected) {
    boolean wasConnected = Connected;
    Connected = isConnected;
    firePropertyChange(MailQueue.pn_Connected, toBoolean(isConnected), toBoolean(wasConnected));
  }

  /**
   *
   **/
  private static final Boolean toBoolean(boolean b) {
    return b ? Boolean.TRUE : Boolean.FALSE;
  }

  /**
   *
   **/
  public boolean isConnected(){
    return Connected;
  }

  /**
   *
   **/
  public abstract void Disconnect();

  /**
   *
   **/
  protected void finalize () throws Throwable {
    Disconnect();
    super.finalize();
  }

}
