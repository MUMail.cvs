/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is" distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.net;

import java.beans.*;
import java.io.*;

/**
 *
 **/
public interface MailClient{

  /**
   *
   **/
  public void addPropertyChangeListener(PropertyChangeListener listener);

  /**
   *
   **/
  public void removePropertyChangeListener(PropertyChangeListener listener);

  /**
   *
   **/
  public void Connect(String host, int port, String userName, String passwd) throws IOException;

  /**
   *
   **/
  public void Disconnect();

  /**
   *
   **/
  public boolean isConnected();

  /**
   *
   **/
  public String GetServerID();

  /**
   *
   **/
  public int getCount() throws IOException;

  /**
   *
   **/
  public int getSize(int index) throws IOException;

  /**
   *
   **/
  public String getUID(int index) throws IOException;

  /**
   *
   **/
  public String[] getHeader(int index) throws IOException;

  /**
   *
   **/
  public String[] getMail(int index, MailListener mailListener) throws IOException, InterruptedException;

  /**
   *
   **/
  public void deleteMail(int index) throws IOException;

  /**
   *
   **/
  public String getProtocolName();

  /**
   *
   **/
  public String getDefaultPort();

}
