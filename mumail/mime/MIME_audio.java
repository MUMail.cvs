/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.mime;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import sun.audio.*;

public class MIME_audio extends MIMEbinary implements ActionListener{

  public MIME_audio() throws InterruptedException {
    super();
  }

  public MIME_audio(String[] mailLine) throws InterruptedException {
    super(mailLine);
  }

  public MIME_audio(String[] mailLine, int headerStart, int bodyStop) throws InterruptedException {
    super(mailLine, headerStart, bodyStop);
  }

  public MIME_audio(mimeHeader Header, String[] mailLine, int bodyStart, int bodyStop) throws InterruptedException {
    super(Header, mailLine, bodyStart, bodyStop);
  }

  protected void createDisplay()
  {
    super.createDisplay();
    // Panel audio = new Panel();
    Button play = new Button("Play");
    play.addActionListener(this);
    add("Center", play);
    // add("Center", audio);
  }

  public void actionPerformed(ActionEvent e){
		
    if(e.getActionCommand().equals("Play")){
      try{
	if(Class.forName("sun.audio.AudioPlayer")!=null){
	  ByteArrayInputStream audio_is = new ByteArrayInputStream(Body);
	  try{
	    AudioStream audio_as = new AudioStream(audio_is);
	    AudioPlayer.player.start(audio_as);
	  } catch (IOException excep){
	  }
	}
      } catch (ClassNotFoundException excep2){
	System.err.println("no undocumented audio package");
      }
    }

    super.actionPerformed(e);
  }
}
