/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.mime;

import java.awt.*;
import java.io.*;
import java.util.Random;

public abstract class MIME extends Panel {

  //  String        ContentTransferEncoding = "7bit";
  mimeHeader    Header;

  Panel customPanel = new Panel();

  public MIME() throws InterruptedException {
    String[] mailLine = new String[0];

    mimeHeader Header = new mimeHeader();
    Initialize(Header, mailLine, 0, 0);
  }

  public MIME(String[] mailLine) throws InterruptedException {
    int bodyStart;
    int bodyStop;

    mimeHeader Header = new mimeHeader(mailLine, 0, mailLine.length);
    bodyStart = Header.getLength() + 1;
    bodyStop = mailLine.length;

    Initialize(Header, mailLine, bodyStart, bodyStop);
  }

  public MIME(String[] mailLine, int headerStart, int bodyStop) throws InterruptedException {
    int bodyStart;

    mimeHeader Header = new mimeHeader(mailLine, headerStart, bodyStop);
    bodyStart = headerStart + Header.getLength() + 1;

    Initialize(Header, mailLine, bodyStart, bodyStop);
  }

  public MIME(mimeHeader Header, String[] mailLine, int bodyStart, int bodyStop) throws InterruptedException {

    Initialize(Header, mailLine, bodyStart, bodyStop);
  }

  public void Initialize(mimeHeader Header, String[] mailLine, int bodyStart, int bodyStop) throws InterruptedException {
    this.Header = Header;
    extractBody(mailLine, bodyStart ,bodyStop);

    // for visual debugging
    // customPanel.setBackground(Color.red);
    customPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));

    createDisplay();
  }

  abstract void createDisplay() throws InterruptedException;

  MIME constructMIME(mimeHeader Header, String[] mailLine, int bodyStart, int bodyStop) throws InterruptedException {

    String majorType = Header.getMajorContent_Type().replace('-', '_');
    String minorType = Header.getMinorContent_Type().replace('-', '_');

    // array of Class to look for right constructor
    Class[] argument_types = new Class[4];

    // Does not work on Netscape 3.0 OS/2 due to buggy Java VM
    //    argument_types[0] = Header.getClass();
    //    argument_types[1] = mailLine.getClass();
    //    argument_types[2] = Integer.TYPE;
    //    argument_types[3] = Integer.TYPE;

    argument_types[0] = mimeHeader.class;
    argument_types[1] = String[].class;
    argument_types[2] = Integer.TYPE;
    argument_types[3] = Integer.TYPE;

    // array of arguments for constructor
    Object[] arguments = new Object[4];
    arguments[0] = Header;
    arguments[1] = mailLine;
    arguments[2] = new Integer(bodyStart);
    arguments[3] = new Integer(bodyStop);

    // class to load
    Class mime_class;

    // try to load the class
    // 1.       MIME_majorType_minorType
    // 2.       MIME_majorType
    // default  MIME_text_plain
    try {
      mime_class = Class.forName("mumail.mime.MIME_" + majorType + "_" + minorType);
    } catch (ClassNotFoundException excep1) {
      try {
	mime_class = Class.forName("mumail.mime.MIME_" + majorType);
      } catch (ClassNotFoundException excep2) {
	try {
	  mime_class = Class.forName("mumail.mime.MIME_text_plain");
	} catch (ClassNotFoundException excep3) {
	  System.err.println(excep3);
	  excep3.printStackTrace();
	  return null;
	}
      }
    }

    java.lang.reflect.Constructor mime_constructor;
    try {
      //ok, class found, get the right constructor
      mime_constructor = mime_class.getConstructor(argument_types);
      // our MIME object
      MIME mime_object;
      // try to instatiate the object
      try {
	mime_object = (MIME) mime_constructor.newInstance(arguments);
	return mime_object;
      } catch (InstantiationException excep5) {
	//problem here: loaded class is interface or abstract class
	System.err.println(excep5);
	excep5.printStackTrace();
	return null;
      } catch (IllegalAccessException excep6) {
	//because we load a class from the right package, "this"! class should
	//have access to the loaded class and his constructor
	System.err.println(excep6);
	excep6.printStackTrace();
	return null;
      } catch (java.lang.reflect.InvocationTargetException excep7) {
	//there was a problem within the constructor of the class
	Throwable excep8 = excep7.getTargetException();
        if (excep8 instanceof InterruptedException) {
          throw (InterruptedException) excep8;
        }
	System.err.println(excep8);
	excep8.printStackTrace();
	return null;
      }
    } catch (NoSuchMethodException excep4) {
      System.err.println(excep4);
      excep4.printStackTrace();
      return null;
    }
  }

  MIME constructMIME(String[] mailLine, int bodyStart, int bodyStop) throws InterruptedException {
    Header = new mimeHeader(mailLine, bodyStart, bodyStop);
    return constructMIME(Header, mailLine, bodyStart + Header.getLength() + 1, bodyStop);
  }


  public mimeHeader getHeader() {
    return(Header);
  }

  abstract void extractBody(String[] mailLine, int bodyStart, int bodyStop) throws InterruptedException ;

  public Panel getCustomPanel(){
    return customPanel;
  }

}
