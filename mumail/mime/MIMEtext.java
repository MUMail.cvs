/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.mime;

import java.awt.*;

public abstract class MIMEtext extends MIME {

  String Body[];

  public MIMEtext() throws InterruptedException {
    super();
  }

  public MIMEtext(String[] mailLine) throws InterruptedException {
    super(mailLine);
  }

  public MIMEtext(String[] mailLine, int headerStart, int bodyStop) throws InterruptedException {
    super(mailLine, headerStart, bodyStop);
  }

  public MIMEtext(mimeHeader Header, String[] mailLine, int bodyStart, int bodyStop) throws InterruptedException {
    super(Header, mailLine, bodyStart, bodyStop);
  }

  abstract String[] extractBodyLines(byte Buffer[]);

  public String[] getLines(){
    return Body;
  }

  void extractBody(String[] mailLine, int bodyStart, int bodyStop) throws InterruptedException {
    String Content_Transfer_Encoding = Header.get("Content-Transfer-Encoding").toLowerCase();
    byte Buffer[];

    if (Content_Transfer_Encoding.equals("quoted-printable")) {
      Buffer = MIMEcoder.decodeQuoted_Printable(mailLine, bodyStart, bodyStop);
    } else if (Content_Transfer_Encoding.equals("base64")) {
      Buffer = MIMEcoder.decodeBase64(mailLine, bodyStart, bodyStop);
    } else if (Content_Transfer_Encoding.equals("8bit")) {
      Buffer = MIMEcoder.decode8Bit(mailLine, bodyStart, bodyStop);
    } else {
      Buffer = MIMEcoder.decode7Bit(mailLine, bodyStart, bodyStop);
    }
    Body = extractBodyLines(Buffer);
  }

}
