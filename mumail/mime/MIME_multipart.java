/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.mime;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

public class MIME_multipart extends MIME implements ItemListener{

  Vector Parts;
  private Panel partpane;

  public MIME_multipart() throws InterruptedException {
    super();
  }

  public MIME_multipart(String[] mailLine) throws InterruptedException {
    super(mailLine);
  }

  public MIME_multipart(String[] mailLine, int headerStart, int bodyStop) throws InterruptedException {
    super(mailLine, headerStart, bodyStop);
  }

  public MIME_multipart(mimeHeader Header, String[] mailLine, int bodyStart, int bodyStop) throws InterruptedException {
    super(Header, mailLine, bodyStart, bodyStop);
  }

  void extractBody(String[] mailLine, int bodyStart, int bodyStop) throws InterruptedException {
    int     i;
    int     j;
    boolean finalBoundaryFound = false;
    MIME    Part;
    String  Boundary = Header.getContent_TypeParameter("boundary");

    Boundary = "--" + Boundary;

    i = bodyStart;
    while (i < bodyStop && !mailLine[i].startsWith(Boundary)) {
      i++;
    }

    j = ++i;
    Parts = new Vector();
    while (j < bodyStop && !finalBoundaryFound) {
      if (mailLine[j].startsWith(Boundary)) {
	Part = constructMIME(mailLine, i, j);
	Parts.addElement(Part);
	i = j + 1;
	finalBoundaryFound = mailLine[j].startsWith(Boundary + "--");
      }  else {
      }
      j++;
    }
  }

  void createDisplay()
  {
    int i;
    int j = 1;
    MIME Part;

    customPanel.setLayout(new CardLayout());

    try {

      Label body_label = new Label("bodyparts:");
      Choice partselect = new Choice();
      partselect.addItemListener(this);

      partpane = new Panel();
      partpane.setLayout(new CardLayout());

      String contentType;

      for (Enumeration e = Parts.elements() ; e.hasMoreElements() ;) {
	Part = (MIME)e.nextElement();

	contentType = Part.getHeader().getContent_Type();

	if (contentType.equals("")) {
	  contentType = "unknown";
	}

	contentType = j++ + ": " + contentType;

	partselect.add(contentType);
	partpane.add(contentType, Part);
        customPanel.add(contentType, Part.getCustomPanel());
      }


      setLayout(new GridLayout(1,1));
      Panel Display_child = new Panel();

      GridBagLayout gb = new GridBagLayout();
      GridBagConstraints gc = new GridBagConstraints();

      Display_child.setLayout(gb);

      gc.fill=GridBagConstraints.HORIZONTAL;
      gc.insets=new Insets(0,0,0,0);
      gc.ipadx=0;
      gc.ipady=0;
      gc.anchor=GridBagConstraints.CENTER;
      gc.weightx=0.0;
      gc.weighty=0.0;
      gc.gridheight=1;
      gc.gridwidth=1;
      gb.setConstraints(body_label, gc);
      Display_child.add(body_label);

      gc.weightx=1.0;
      gc.gridwidth=GridBagConstraints.REMAINDER;
      gb.setConstraints(partselect, gc);
      Display_child.add(partselect);

      gc.gridheight=GridBagConstraints.REMAINDER;
      gc.fill=GridBagConstraints.BOTH;
      gc.weightx=1.0;
      gc.weighty=1.0;
      gb.setConstraints(partpane, gc);
      Display_child.add(partpane);

      add(Display_child);
    } catch (Exception e) {
      System.out.println(e);
      e.printStackTrace();
    }
  }

  public void itemStateChanged(ItemEvent e){
    ((CardLayout)partpane.getLayout()).show(partpane,(String)e.getItem());
    ((CardLayout)customPanel.getLayout()).show(customPanel,(String)e.getItem());
  }

  public int getPartCount(){
    return Parts.size();
  }

  public MIME getPart(int i){
    if(i<0 || i>=getPartCount()){
      //IllegalArgumentException
      return null;
    }
    else{
      return (MIME) Parts.elementAt(i);
    }
  }

}
