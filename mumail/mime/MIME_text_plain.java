/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.mime;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class MIME_text_plain extends MIMEtext implements ItemListener {

  TextArea TextPanel;
  Choice FontChoice;

  public MIME_text_plain() throws InterruptedException {
    super();
  }

  public MIME_text_plain(String[] mailLine) throws InterruptedException {
    super(mailLine);
  }

  public MIME_text_plain(String[] mailLine, int headerStart, int bodyStop) throws InterruptedException {
    super(mailLine, headerStart, bodyStop);
  }

  public MIME_text_plain(mimeHeader Header, String[] mailLine, int bodyStart, int bodyStop) throws InterruptedException {
    super(Header, mailLine, bodyStart, bodyStop);
  }

  protected String[] extractBodyLines(byte Buffer[]) {
    int i;
    String           currentLine;
    java.util.Vector lineVector = new java.util.Vector();
    BufferedReader   Reader;
    String[]         Line;
    String           ContentTypeHeader;
    String           Charset;
    String           Encoding;

    Charset = Header.getContent_TypeParameter("charset").toLowerCase();

    if(Charset.compareTo("us-ascii") == 0) {
      Charset = "iso-8859-1";
    }

   Encoding = CharacterEncoding.aliasName(Charset);
   if (Encoding == null) {
     Encoding = Charset;
   }


//    System.err.println("Using encoding '" + Encoding + "'.");

    try {
      Reader = new BufferedReader( new InputStreamReader(new ByteArrayInputStream(Buffer), Encoding));
    } catch (java.lang.Exception e1) {
      Reader = new BufferedReader( new InputStreamReader(new ByteArrayInputStream(Buffer)));
      System.err.println("'" + Encoding + "' encoding unknown - using iso-8859-1");
    }
    try {
      while(null != (currentLine = Reader.readLine())) {
	lineVector.addElement(currentLine);
      }
    } catch (IOException e2) {
      System.err.println(e2);
      e2.printStackTrace();
    }

    Line = new String[lineVector.size()];
    lineVector.copyInto(Line);
    return(Line);
  }

  protected void createDisplay() throws InterruptedException {
    TextPanel = new TextArea("", 0, 0, TextArea.SCROLLBARS_VERTICAL_ONLY);
    int i;

    if(mumail.MUMail.showFontChoice){
      FontChoice = new Choice();

      String[] fontlist = Toolkit.getDefaultToolkit().getFontList();
      for(i=0;i<fontlist.length;i++){
        FontChoice.add(fontlist[i]);
      }
      FontChoice.addItemListener(this);
    }

    TextPanel.setEditable(false);
    TextPanel.setBackground(new Color(255,255,255));
    for(i = 0; i < Body.length; i++) {
      if (Thread.currentThread().interrupted()) {
        throw new InterruptedException();
      }
      TextPanel.append(Body[i] + "\n");
    }

    setLayout(new BorderLayout());

    if(mumail.MUMail.showFontChoice){
      customPanel.add(FontChoice);
    }
    add(TextPanel);
  }

  public void itemStateChanged(ItemEvent e){
    if(e.getSource()==FontChoice){
      TextPanel.setFont(new Font(FontChoice.getSelectedItem(), TextPanel.getFont().getStyle(), TextPanel.getFont().getSize()));
    }
  }


}
