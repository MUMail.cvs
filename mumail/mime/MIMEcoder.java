/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.mime;

import java.io.*;

public class MIMEcoder {

  static final int toBase64Table[] = {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
    'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
    'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
    'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
    'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
    'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7',
    '8', '9', '+', '/'
  };
  static int fromBase64Table[] = new int[256];

  static {
    int i;

    for ( i = 0; i < 256; i++) {
      fromBase64Table[i] = -1;
    }
    for ( i = 0; i < 64; i++) {
      fromBase64Table[toBase64Table[i]] = i;
    }
  }

  public static byte[] decodeQuoted_Printable(String[] Line, int startIndex, int stopIndex) throws InterruptedException {
    int                   i, j, l;
    boolean               softLineBreak = false;
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();

    for (i = startIndex; i < stopIndex; i++) {
      if (Thread.currentThread().interrupted()) {
        throw new InterruptedException();
      }
      l = Line[i].length();
      for (j = 0; j < l; j++) {
        if (Line[i].charAt(j) != '=') {
          buffer.write((int) Line[i].charAt(j));
        } else if (j == l - 1) {
          softLineBreak = true;
        } else if (j == l - 2) {
          buffer.write((int) Line[i].charAt(j));
        } else {
          buffer.write(Integer.valueOf(Line[i].substring(j + 1, j + 3).toLowerCase(), 16).intValue());
          j+= 2;
        }
      }
      if (softLineBreak) {
    softLineBreak = false;
      } else {
        buffer.write('\r'); buffer.write('\n');
      }
    }

    return buffer.toByteArray();
  }


  public static byte[] decodeBase64(String[] Line, int startIndex, int stopIndex) throws InterruptedException {

    int          i, j, l;
    int          Value = 0;
    int          Bits = 0;
    int          encoded;
    ByteArrayOutputStream buffer = new ByteArrayOutputStream(Line.length * 54 );

    for( i = startIndex; i < stopIndex; i++) {
      if (Thread.currentThread().interrupted()) {
        throw new InterruptedException();
      }
      l = Line[i].length();
      for( j = 0; j < l;) {
        encoded = (int) (Line[i].charAt(j++));
        Value = (Value << 6) | fromBase64Table[encoded];
        if (Bits >= 2 ) {
          Bits -= 2;
          buffer.write(Value >> Bits);
          Value &= ((1 << Bits) - 1);
        } else {
          Bits += 6;
        }
      }
    }

    return buffer.toByteArray();
  }


  public static byte[] decode7Bit(String[] Line, int startIndex, int stopIndex) throws InterruptedException {
    return decode8Bit(Line, startIndex, stopIndex);
  }

  public static byte[] decode8Bit(String[] Line, int startIndex, int stopIndex) throws InterruptedException {
    int              i, j, l;
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();

    for (i = startIndex; i < stopIndex; i++) {
      if (Thread.currentThread().interrupted()) {
        throw new InterruptedException();
      }
      l = Line[i].length();
      for ( j = 0; j < l; j++) {
        buffer.write((int) Line[i].charAt(j));
      }
      buffer.write('\r'); buffer.write('\n');
    }

    return buffer.toByteArray();
  }



}
