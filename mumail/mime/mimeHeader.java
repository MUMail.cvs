/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.mime;

public class mimeHeader {

  String[] headerLine;
  int      headerStart;
  int      headerStop;

  public mimeHeader() {
    headerLine = new String[0];
    headerStart = 0;
    headerStop = headerStart;
  }

  public mimeHeader(String[] mailLine) {

    headerLine = mailLine;
    headerStart = 0;
    headerStop = headerStart;

    while(!headerLine[headerStop].equals("") && headerStop<mailLine.length) {
      headerStop++;
    }
  }

  public mimeHeader(String[] mailLine, int startIndex, int stopIndex) {

    headerLine = mailLine;
    headerStart = startIndex;
    headerStop = headerStart;

    while(!headerLine[headerStop].equals("") && headerStop<stopIndex) {
      headerStop++;
    }
  }

  public int getLength() {
    return(headerStop - headerStart);
  }

  public String get(String headerName) {
    int i;
    String Value = "";
    String lowerHeaderName = headerName.toLowerCase();

    for(i = headerStart; i < headerStop; i++) {
      if (headerLine[i].toLowerCase().startsWith(lowerHeaderName + ":")) {
	Value = headerLine[i].substring(headerName.length() + 1).trim();
	while (i + 1 < headerStop && (headerLine[i + 1].startsWith (" ") || headerLine[i + 1].startsWith ("\t") ) ) {
	  Value = Value + " " + headerLine[++i].trim();
	}
      }
    }
    return Value;
  }


  public String getContent_Type() {
    String contentTypeField = get("Content-Type");
    String contentType;
    int i = contentTypeField.indexOf(';');

    if (i == -1) {
      contentType = contentTypeField.trim().toLowerCase();
    } else {
      contentType = contentTypeField.substring(0,i).trim().toLowerCase();
    }

    if (contentType.equals("")) {
      contentType = "text/plain";
    }

    return contentType;

  }


  public String getMajorContent_Type(){ 

    String majorType;
    String contentType = getContent_Type();
    int i = contentType.indexOf('/');

    if (i == -1) {
      majorType = contentType;
    } else {
      majorType = contentType.substring(0, i);
    }

    return majorType;
  }		


  public String getMinorContent_Type(){ 

    String minorType;
    String contentType = getContent_Type();
    int i = contentType.indexOf('/');

    if (i == -1) {
      minorType = "";
    } else {
      minorType = contentType.substring(i + 1, contentType.length());
    }

    return minorType;
  }		


  public String getContent_TypeParameter(String Name) {
    String Content_TypeField = get("Content-Type");
    String Value = "";
    boolean found = false;
    int i = Content_TypeField.indexOf(';');
    int j;
    
    while (!(found || (i == -1))) {
      Content_TypeField = Content_TypeField.substring(i + 1).trim();
      if (Content_TypeField.startsWith(Name)) {
	Value = Content_TypeField.substring(Name.length()).trim();
	if (Value.startsWith("=")) {
	  found = true;
	  Value = Value.substring(1).trim();
	  if (Value.startsWith("\"")) {
	    j = Value.indexOf("\"", 1);
	    if ( j != -1) {
	      Value = Value.substring(1, j);
	    }
	  } else {
	    j = Value.indexOf(';');
	    if (j != -1) {
	      Value = Value.substring(0, j).trim();
	    }
	  }
	}
      }
      i = Content_TypeField.indexOf(';');
    }
    return Value;
  }
  
  public String getFrom() {
    return get("From");
  }

  public String getTo() {
    return get("To");
  }

  public String getSubject() {
    return get("Subject");
  }

  public String getDate() {
    return get("Date");
  }

}
