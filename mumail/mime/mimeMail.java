/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.mime;


public class mimeMail extends MIME_message_rfc822 {

  public mimeMail(String mailLine[]) throws InterruptedException {
    // Betrachtet man eine mail als message/rfc822 MIME-Typ
    // sind die Header MIME-Header und zugleich Teil der rfc822
    // Mail. Deshalb muss der bodyStart auf 0 und nicht hinter
    // den Header gesetzt werden!!!

    super(new mimeHeader(mailLine, 0, mailLine.length), mailLine, 0, mailLine.length);
  }
}
