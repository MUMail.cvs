/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.mime;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

public abstract class MIMEbinary extends MIME implements ActionListener{

  byte[] Body;

  public MIMEbinary() throws InterruptedException {
    super();
  }

  public MIMEbinary(String[] mailLine) throws InterruptedException {
    super(mailLine);
  }

  public MIMEbinary(String[] mailLine, int headerStart, int bodyStop) throws InterruptedException {
    super(mailLine, headerStart, bodyStop);
  }

  public MIMEbinary(mimeHeader Header, String[] mailLine, int bodyStart, int bodyStop) throws InterruptedException {
    super(Header, mailLine, bodyStart, bodyStop);
  }

  void extractBody(String[] mailLine, int bodyStart, int bodyStop) throws InterruptedException {
    String Content_Transfer_Encoding = Header.get("Content-Transfer-Encoding");

    if (Content_Transfer_Encoding.equals("quoted-printable")) {
      Body = MIMEcoder.decodeQuoted_Printable(mailLine, bodyStart, bodyStop);
    } else if (Content_Transfer_Encoding.equals("base64")) {
      Body = MIMEcoder.decodeBase64(mailLine, bodyStart, bodyStop);
    } else if (Content_Transfer_Encoding.equals("8bit")) {
      Body = MIMEcoder.decode8Bit(mailLine, bodyStart, bodyStop);
    } else {
      Body = MIMEcoder.decode7Bit(mailLine, bodyStart, bodyStop);
    }
  }

  void createDisplay()
  {
    setLayout(new BorderLayout(5,5));
    setBackground(Color.white);

    Panel infopane = new Panel();
    infopane.setLayout(new FlowLayout(FlowLayout.CENTER,5,5));

    Label type_label = new Label(Header.getContent_Type()+":");
    Label name_label = new Label(Header.getContent_TypeParameter("filename") + Header.getContent_TypeParameter("name"));
    infopane.add(type_label);
    infopane.add(name_label);

    if(!mumail.MUMail.applet){
      Button save = new Button("Save");
      save.addActionListener(this);
      save.setActionCommand("I18N_SAVE");
      customPanel.add(save);
    }

    add("North", infopane);
  }

  public void actionPerformed(ActionEvent e){
    if(e.getActionCommand().equals("I18N_SAVE")){
      Component parent = this;
      while(parent.getParent()!=null){
	parent=parent.getParent();
	if(parent instanceof Frame){
	  FileDialog fdialog = new FileDialog((Frame)parent, "Saving...", FileDialog.SAVE);
	  fdialog.setFile(Header.getContent_TypeParameter("filename") + Header.getContent_TypeParameter("name"));
	  fdialog.show();
	  if(fdialog.getFile()!=null){
	    //System.out.println(fdialog.getDirectory() + fdialog.getFile());
	    try{
	      FileOutputStream out = new FileOutputStream(fdialog.getDirectory() + fdialog.getFile());
	      out.write(Body);
	      out.close();
	    } catch (IOException excep) {
	      System.err.println(excep.getMessage());
	    }
	  }
	  break;
	}
      }
    }
  }

}
