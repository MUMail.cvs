/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.mime;

import java.awt.*;

public class MIME_image extends MIMEbinary{

  public MIME_image() throws InterruptedException {
    super();
  }

  public MIME_image(String[] mailLine) throws InterruptedException {
    super(mailLine);
  }

  public MIME_image(String[] mailLine, int headerStart, int bodyStop) throws InterruptedException {
    super(mailLine, headerStart, bodyStop);
  }

  public MIME_image(mimeHeader Header, String[] mailLine, int bodyStart, int bodyStop) throws InterruptedException {
    super(Header, mailLine, bodyStart, bodyStop);
  }

  protected void createDisplay() {
    super.createDisplay();
    Image image=null;

    try{
      image = Toolkit.getDefaultToolkit().createImage(Body);
    } catch (Exception e){
      System.err.println(e);
      e.printStackTrace();
    }

    if(image!=null) {
      //Display = new mumail.gui.ImageViewer(image);
      ScrollPane scrollpane = new ScrollPane(ScrollPane.SCROLLBARS_AS_NEEDED);
      scrollpane.add("Center", new mumail.gui.ImageViewer(image));
      add("Center", scrollpane);
    }
  }

}
