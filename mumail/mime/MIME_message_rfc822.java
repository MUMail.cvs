/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail.mime;

import java.lang.reflect.Array;
import java.awt.*;
import mumail.gui.HeaderPanel;

public class MIME_message_rfc822 extends MIME {

  MIME Body;
  mimeHeader MailHeader;

  public MIME_message_rfc822()  throws InterruptedException{
    super();
  }

  public MIME_message_rfc822(String[] mailLine) throws InterruptedException {
    super(mailLine);
  }

  public MIME_message_rfc822(String[] mailLine, int headerStart, int bodyStop) throws InterruptedException {
    super(mailLine, headerStart, bodyStop);
  }

  public MIME_message_rfc822(mimeHeader Header, String[] mailLine, int bodyStart, int bodyStop) throws InterruptedException {
    super(Header, mailLine, bodyStart, bodyStop);
  }

  void extractBody(String mailLine[], int bodyStart, int bodyStop) throws InterruptedException {
    MailHeader = new mimeHeader(mailLine, bodyStart, bodyStop);
    Body = constructMIME(MailHeader, mailLine, bodyStart + MailHeader.getLength() + 1, bodyStop);
  }

  public MIME getBody(){
    return Body;
  }


  void createDisplay() {
    GridBagLayout      gb = new GridBagLayout();
    GridBagConstraints gc = new GridBagConstraints();

    setLayout(gb);

    HeaderPanel hp=new HeaderPanel(MailHeader.getFrom(), MailHeader.getTo(), MailHeader.getSubject(), MailHeader.getDate());

    gc.fill=GridBagConstraints.HORIZONTAL;
    gc.insets=new Insets(0,0,0,0);
    gc.ipadx=0;
    gc.ipady=0;
    gc.anchor=GridBagConstraints.CENTER;
    gc.weightx=1.0;
    gc.weighty=0.0;
    gc.gridwidth=GridBagConstraints.REMAINDER;
    gc.gridheight=1;
    gb.setConstraints(hp, gc);
    add(hp);

    gc.fill=GridBagConstraints.BOTH;
    gc.weighty=1.0;
    gc.weightx=1.0;
    gb.setConstraints(Body, gc);
    add(Body);
    customPanel.add(Body.getCustomPanel());

  }

}
