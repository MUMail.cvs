/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package mumail;

import java.awt.*;
import java.net.*;
import java.awt.event.*;
import java.beans.*;
import java.applet.Applet;
import java.io.IOException;
import mumail.gui.*;
import mumail.net.*;
import musoft.utils.*;

public class MUMail extends Applet implements ActionListener, PropertyChangeListener {


  public static final int HiVersion  = 1;
  public static final int MedVersion = 4;
  public static final int LowVersion = 2;
  public static final String ID = "MUMail " + HiVersion + "." + MedVersion + "." + LowVersion;

  public static final String LOGIN_PANEL = "LoginPanel";
  public static final String SPLASH_PANEL = "SplashPanel";
  public static final String SENDMAIL_PANEL = "SendMailPanel";
  public static final String MAIL_PANEL = "MailPanel";
  public static final String MESSAGE_PANEL = "MessagePanel";

  public static final String Homepage = "http://www.musoft.de/MUMail";

  // configurable options
  private static String ServerHost = "";
  private static String ServerPort = "";
  private static String SmtpHost = "";
  private static String SmtpPort = "";
  private static String User = "";
  private static String MailDomain = "";
  private static String MailAddress = "";
  private static String Password = "";
  public static boolean novice = false;
  public static boolean showFontChoice = false;
  public static boolean authorizedSend = false;
  public static int headerListRows = 5;
  public static boolean protocolSelect = false;

  Timer timer;
  LoginPanel lp;
  MailPanel mp;
  MessagePanel messp;
  SplashPanel splash;
  SendMailPanel smp;
  MailQueue mq;
  SmtpClient sm;
  boolean inSplash = false;
  String[] argv;

  MailClientFactory factory;
  
  public static boolean applet = true;

  public static void main(String[] argv) {

    MUMail app = new MUMail();
    app.argv = argv;

    app.applet = false;
    app.init();

    Frame f = new Frame ("MUMail");
    f.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent event) {
      	System.exit(0);
      }
    });

    Insets i = f.getInsets();
    f.setSize(app.getPreferredSize().width+i.right+i.left, app.getPreferredSize().height+i.bottom+i.top);
    f.setBackground(Color.lightGray);
    f.add(app);
    f.show(); 

    app.start();
  }

  
  public void init() {
    //System.setErr(System.out);
    System.out.println(getAppletInfo());
    setLayout(new CardLayout(0,0));

    // init splash Panel
    Image image;
    String name = "MUMail.gif";
    timer = new Timer(5000, new TimerAdapter(){
      public void timerAlarm() {
      	switchFromSplashToLogin();
      }
    });
    if(applet){
      image = getImage(getCodeBase(), name);
    }
    else{
      image = java.awt.Toolkit.getDefaultToolkit().getImage(name);
    }
    splash = new SplashPanel(image, timer);
    //((CardLayout)getLayout()).addLayoutComponent(splash, SPLASH_PANEL);
    add(SPLASH_PANEL, splash);
    splash.addMouseListener(new MouseAdapter(){
      public void mouseClicked(MouseEvent e){
        if(timer!=null){
      	  timer.cancel();
        }
        switchFromSplashToLogin();
      }
    });
    showPanel(SPLASH_PANEL);
    inSplash = true;

    // init textfields in login panel
    if(applet){
      initApplet();
    }
    else{
      initApplication();
    }

    // init login panel
    factory = new MailClientFactory("mumail.net.Pop3Client:mumail.net.Imap4Client");
    lp = new LoginPanel(ServerHost, ServerPort, SmtpHost, SmtpPort, User, Password, this, factory);
    add(LOGIN_PANEL, lp);

  }
  
  public void switchFromSplashToLogin(){
    if(inSplash){ 
      inSplash = false;
      boolean allParametersThere = ! (lp.getPassword().equals("") || lp.getUser().equals("") || lp.getServerHost().equals("") || lp.getSmtpHost().equals("") || lp.getServerPort().equals("") || lp.getSmtpPort().equals(""));
      if(allParametersThere){
        Connect();
      }
      else{
        showPanel(LOGIN_PANEL); 
      }
      timer = null;  
    }   
  }

  public synchronized void stop() {
    if (mq != null) {
	mq.Close();
    }
  }

  private String getParam(String Param, String Default){
    String dummy = getParameter(Param);
    if(dummy==null){
      dummy = Default;
    }
    return dummy;
  }

  public void initApplet(){
    ServerHost = (getParam("ServerHost", getCodeBase().getHost()));
    SmtpHost = (getParam("SmtpHost", ServerHost));
    ServerPort = (getParam("ServerPort", "110"));
    SmtpPort = (getParam("SmtpPort", "25"));
    User = (getParam("User", ""));
    MailDomain = (getParam("MailDomain", ""));
    MailAddress = (getParam("MailAddress", ""));
    Password = (getParam("Password", ""));
    showFontChoice = Boolean.valueOf(getParam("showFontChoice", "false")).booleanValue();
    novice = Boolean.valueOf(getParam("novice", "false")).booleanValue();
    protocolSelect = Boolean.valueOf(getParam("protocolSelect", "false")).booleanValue();
    authorizedSend = Boolean.valueOf(getParam("authorizedSend", "false")).booleanValue();
    try{
      headerListRows = Integer.parseInt(getParam("headerListRows", "5"));
    } catch (NumberFormatException e){
    }
    if(headerListRows<2 || headerListRows>20){
      headerListRows = 5;
    }
  }

  public void initApplication(){
    ServerHost = ("mail");
    SmtpHost = ("mail");
    ServerPort = ("110");
    SmtpPort = ("25");
    User = (System.getProperty("user.name"));
    Password = ("");
    for (int i = 0; i < argv.length; i++) {
      if (argv[i].toLowerCase().equals("-pophost") ||
	  argv[i].toLowerCase().equals("-serverhost")) {
        ServerHost = (argv[++i]);
      } else if (argv[i].toLowerCase().equals("-smtphost")) {
        SmtpHost = (argv[++i]);
      } else if (argv[i].toLowerCase().equals("-popport") ||
		 argv[i].toLowerCase().equals("-serverport")) {
        ServerPort = (argv[++i]);
      } else if (argv[i].toLowerCase().equals("-smtpport")) {
        SmtpPort = (argv[++i]);
      } else if (argv[i].toLowerCase().equals("-user")) {
        User = (argv[++i]);
      } else if (argv[i].toLowerCase().equals("-maildomain")) {
        MailDomain = (argv[++i]);
      } else if (argv[i].toLowerCase().equals("-mailaddress")) {
        MailAddress = (argv[++i]);
      } else if (argv[i].toLowerCase().equals("-password")) {
        Password = (argv[++i]);
      } else if (argv[i].toLowerCase().equals("-showfontchoice")) {
        showFontChoice = true;
      } else if (argv[i].toLowerCase().equals("-authorizedsend")) {
        authorizedSend = true;
      } else if (argv[i].toLowerCase().equals("-novice")) {
        novice = true;
      } else if (argv[i].toLowerCase().equals("-headerlistrows")) {
	try{
          headerListRows = Integer.parseInt(argv[++i]);
        } catch(NumberFormatException e){
        }
        if(headerListRows<2 || headerListRows>20){
          headerListRows = 5;
        }
      } else if (argv[i].toLowerCase().equals("-protocolselect")) {
        protocolSelect = true;
      } else {
        System.err.println("usage: java mumail.MUMail [-smtphost   <SMTP Server>] |");
        System.err.println("                          [-serverhost <Mail Server>] |");
        System.err.println("                          [-smtpport   <SMTP Port>] |");
        System.err.println("                          [-serverport    <Mail Port>] |");
        System.err.println("                          [-user       <User Name>] |");
        System.err.println("                          [-password   <Password>] |");
	System.err.println("                          [-from       <my address>] |");	
	System.err.println("                          [-maildomain <domain part of email adress>] |");
	System.err.println("                          [-headerListRows <number>] |");
        System.err.println("                          [-showFontChoice] |");
        System.err.println("                          [-authorizedSend] |");
        System.err.println("                          [-novice]");
        System.err.println("                          [-protocolSelect]");
	System.exit(1);
      }
    }  
  }

  public void showPanel(String s){
    //System.out.println("showPanel("+s+")");
    ((CardLayout)getLayout()).show(this, s);
  }

  public void showLastPanel(){
    ((CardLayout)getLayout()).previous(this);
  }

  public void newMessagePanel(String s){
    messp = new MessagePanel(s, this);
    add(MESSAGE_PANEL, messp);
    showPanel(MESSAGE_PANEL);
  }

  /*public void newMailPanel() throws IOException {
    mp = new MailPanel(mq, this);
    add(MAIL_PANEL,mp);
    showPanel(MAIL_PANEL);
    mp.DisplayHeaders();
    }*/
  
  public void newSendMailPanel(boolean reply){

    String Domain;
    String User = lp.getUser();

    if(MailAddress.equals("")){
      if (MailDomain.equals("")) {
	Domain = lp.getServerHost();
	int i = Domain.indexOf(".");
	if (i == -1) {
	  if (Domain.equals("") || Domain.equals("localhost")) {
	    Domain = "";
	  } else {
	    Domain = "@" + Domain;
	  }
	} else {
	  Domain = "@" + Domain.substring(i + 1);
	}
      }else {
	Domain = "@" + MailDomain;
      }
      User = User + Domain;
    }
    else{
      User = MailAddress;
    }

    if(reply){
      smp = new SendMailPanel(mp.mo, User, this);
    }
    else{
      smp = new SendMailPanel(User, this);
    }
    //CardLayout)getLayout()).addLayoutComponent(smp, SENDMAIL_PANEL);
    add(SENDMAIL_PANEL, smp);
    showPanel(SENDMAIL_PANEL);
  }

  public void Connect(){
    setCursor(new Cursor(Cursor.WAIT_CURSOR));
    try {
      MailClient mc = factory.getMailClientByProtocolName(lp.getProtocol());
      System.err.println(mc);
      mq = new MailQueue(mc, lp.getServerHost(), java.lang.Integer.parseInt(lp.getServerPort()), lp.getUser(), lp.getPassword());
      mq.addPropertyChangeListener(this);
      //newMailPanle();
      mq.Open();
      mp = new MailPanel(mq, this);
      if(applet){
	showStatus("connected to " + lp.getServerHost() + " as " + lp.getUser());
      }
      add(MAIL_PANEL,mp); 
      showPanel(MAIL_PANEL); 
      mp.DisplayHeaders(); 
    } catch (java.lang.Exception e2) {
      //System.err.println(e2);
      //e2.printStackTrace();
      newMessagePanel(e2.getMessage());
    }
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
    if (propertyChangeEvent.getSource() == mq) {
      if (propertyChangeEvent.getPropertyName().equals(mq.pn_Connected)) {
      }
    }
  }

  public void actionPerformed(ActionEvent e) {
    // confirm message panel
    if (e.getActionCommand().equals("OK")){
      showLastPanel();
      remove(messp); 
    }

    // from login panel
    if ((e.getActionCommand().equals("Connect"))||(e.getSource()==lp.PasswordTF)){ 
      Connect();
    }

    // from mail panel
    if (e.getActionCommand().equals("Connection")) {
      if (mq.isConnected()) {
        mq.Close();
      } else {
        try{
          mq.Open();
        } catch(IOException exep) {
  	  exep.printStackTrace();
        }
      }
    }

    // from mail panel
    if (e.getActionCommand().equals("Close")) {
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      mq.Close();
      remove(mp);
      mp = null;
      if(applet){
      	showStatus("MUMail login");
      }
      lp.setPassword("");
      showPanel(LOGIN_PANEL);
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    // from mail panel
    if (e.getActionCommand().equals("Reply")){
      newSendMailPanel(true);
    }

    // from mail panel AND!!! login panel
    if (e.getActionCommand().equals("Send")){
      newSendMailPanel(false);
    }

    // from sendmailpanel, means send mail now
    if (e.getActionCommand().equals("SendMail")){
      try {
      	sm = new SmtpClient(lp.getSmtpHost(), java.lang.Integer.parseInt(lp.getSmtpPort()));
	sm.submitMail(smp.From.getText(), smp.To.getText(), smp.Subject.getText(), smp.Text.getText(), smp.getEncoding());
        showLastPanel();
        remove(smp);
	smp = null;
      } catch (java.lang.Exception exep) {
        //remove(smp);
	newMessagePanel(exep.getMessage());
	//System.err.println(exep);
	//exep.printStackTrace();	
      }
    }

    // from sendmailpanel
    if (e.getActionCommand().equals("Cancel")){
      showLastPanel();
      remove(smp);
      smp = null;
    }

    // from loginpanel
    if (e.getActionCommand().equals(mumail.gui.LoginPanel.homepage)){
      try{
        getAppletContext().showDocument(new URL(Homepage));
      }
      catch (MalformedURLException excep){
      }
    }

    // from loginpanel
    if (e.getActionCommand().equals("Exit")){
      System.exit(0);
    }

    // from mailpanel
    if (e.getActionCommand().equals("Exit2")) {
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      mq.Close();
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      System.exit(0);
    }
  }
  
  public String[][] getParameterInfo() {
    String[][] info = {
      {"ServerHost",        "String",  ""},
      {"SmtpHost",          "String",  ""},
      {"ServerPort",        "String",  ""},
      {"SmtpPort",          "String",  ""},
      {"User",              "String",  ""},
      {"Password",          "String",  ""},
      {"MailDomain",        "String",  ""},
      {"showFontChoice",    "booelan", ""},
      {"novice",            "boolean", ""},
      {"authorizedSend",    "boolean", ""},
      {"headerListRows",    "Integer", ""},
      {"protocolSelect",    "boolean", ""},
    };
    return info;
  }

  public String getAppletInfo() {
    return ID + " Copyright 1997 - 2001 by Uli Luckas  <luckas@cs.tu-berlin.de>\n" +
      "                                     Mark T\u00fcmpfel <marktop@cs.tu-berlin.de>\n" +
      "Homepage: "+Homepage+"\n"+
      "MUMail is free software under the terms of GNU General Public License\n" +
      "and comes with ABSOLUTELY NO WARRANTY.\n";
  }

  public Dimension getPreferredSize(){
    return new Dimension(600,440);
  }

}
