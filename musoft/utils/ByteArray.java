/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package musoft.utils;

import java.io.*;
import java.util.Vector;

public class ByteArray {

  public static byte[] textToByteArray(String[] textLine, int startIndex, int stopIndex) {
    int    i;
    int    lastLine = Math.min(textLine.length, stopIndex);
    int    byteSize = 0;
    ByteArrayOutputStream bs;
    BufferedWriter os;

    for (i = 0; i < lastLine; i++) {
      byteSize = byteSize + textLine[i].length() + 2;
    }
    
    try {

    bs = new ByteArrayOutputStream(byteSize);
    os = new BufferedWriter(new OutputStreamWriter(bs, "8859_1"));
    for (i = startIndex; i < lastLine; i++) {
      os.write(textLine[i], 0, textLine[i].length());
      os.write("\r\n");
    }
    os.close();
    return (bs.toByteArray());

    } catch (IOException e) {

      System.err.println(e);
      e.printStackTrace();

      return null;

    }
  }

  public static byte[] textToByteArray(String[] textLine) {
    return textToByteArray(textLine, 0, textLine.length);
  }

  public static String[] byteArrayToText(byte[] textByte, int startIndex, int stopIndex) {

    BufferedReader br;
    String         currentLine;
    Vector         stringBuffer = new Vector();
    String[]       stringArray;
    
    try {

      br = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(textByte, startIndex, stopIndex - startIndex), "8859_1"));

      while ((currentLine = br.readLine()) != null) {
	stringBuffer.addElement(currentLine);
      }
      
      stringArray = new String[stringBuffer.size()];
      stringBuffer.copyInto(stringArray);

      return stringArray;
    } catch (IOException e) {

      System.err.println(e);
      e.printStackTrace();

      return null;

    }
  }

  public static String[] byteArrayToText(byte[] textByte) {
    return byteArrayToText(textByte, 0, textByte.length);
  }

}
