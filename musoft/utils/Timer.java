/*
 * Copyright (C) 1998-2001 Mark Tuempfel and Uli Luckas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * To reach the Authors you can send E-Mail to:
 *	Mark Tuempfel <marktop@cs.tu-berlin.de>
 *      Uli Luckas    <luckas@cs.tu-brelin.de>
 *
 */

package musoft.utils;

import java.lang.reflect.*;

public class Timer extends Thread {
 
  long milliseconds;
  TimerAdapter callback;
  boolean isSleeping;
  boolean isCanceld;
	     
  public Timer(int milliseconds, TimerAdapter callback) {
    isSleeping = false;
    isCanceld  = false;
    this.milliseconds = milliseconds;
    this.callback = callback;
  }

  public void cancel(){
    if(isSleeping){
      this.interrupt();
    }
    isCanceld = true;
  }

  public void run() {
    try {
      if(!isCanceld){
        isSleeping = true;
        sleep(milliseconds); 
        isSleeping = false;
        //System.out.println("calling caller");
        callback.timerAlarm();
      }
    }
    catch (InterruptedException e){
      isSleeping = false;
      //System.out.println("canceled!");
    }
  }
}
